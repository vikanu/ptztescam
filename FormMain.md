 public Form1()
        {
            InitializeComponent();

            //initialize tag button
            btnLeft.Tag = MoveDirection.Left;
            btnRight.Tag = MoveDirection.Right;
            btnUp.Tag = MoveDirection.Up;
            btnDown.Tag = MoveDirection.Down;
            btnUpLeft.Tag = MoveDirection.LeftUp;
            btnUpRight.Tag = MoveDirection.RightUp;
            btnDownLeft.Tag = MoveDirection.LeftDown;
            btnDownRight.Tag = MoveDirection.RightDown;

            UsrCtrl1.Location = new System.Drawing.Point(0, 0);
            UsrCtrl1.Width = pnlMenuFloating.Width;
            UsrCtrl1.Height = pnlMenuFloating.Height;

            
            pnlMenuFloating.Controls.Add(UsrCtrl1);
        }

code diatas untuk menginisialisasi tag button(Left, Right, Up, Down, UpLeft, UpRight,DownLeft, DownRight).
membuat usercontrol baru perintah ini mengarah pada [Panel Control User](ClassUserControl.md)


private void MouseDownMove(object sender, MouseEventArgs e)
        {
            var button = sender as Button;
            var moveDirection = (MoveDirection)((Button)sender).Tag;

            if (ActiveCamera != null)
            {
                ActiveCamera.Move(moveDirection);

                listBox1.Items.Add(ActiveCamera.CameraName + " : Direction Move " + moveDirection.ToString());
            }
        }

code diatas diguanakan untuk memanggil fungsi button, fungsi moveDirection diletakkan ketika event MouseDownMove.
