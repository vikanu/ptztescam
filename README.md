## Overview
Akses ipcamera menggunakan Library Ozeki Camera SDK dan menggunakan local jaringan


### Hardware Uji Fungsi:
1.	Dome Camera (PTZ Camera) - HIK
2.	Network Bullet Camera – HIK

### Softaware Uji Fungsi:
1.	Windows 7 ultimate
2.	Visual Studio 2015 (minimal) 

Hasil  :
-	Dome Camera(Camera PTZ)
 Fungsi PTZ (Right, DownRight, Down, DownLeft, Left, UpLeft, Up, UpRight): button yang diatas dicoba sudah bisa, camera gerak sesuai button direction.
-	Dome Untuk Camera PTZ tidak bisa load gambar pada form.
-	Untuk menampilkan 2 kamera/ lebih pada 1 project, masih tetap belum bisa.

### Packages
Nuget OzekiCamera

- using Ozeki.Camera
- using Ozeki.Media

### Installing

 \bin\Debug\TesPTZ.exe 

### 

1. [Add Camera](https://bitbucket.org/vikanu/ptztescam/src/master/AddCamera.md)
2. [Camera Connect](classConnect.md)
3. [Panel Camera](ClassPanelMenuCamera.md)
4. [Panel Control User](ClassUserControl.md)
5. [Form Main](FormMain.md)


