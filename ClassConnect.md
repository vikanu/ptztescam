public partial class frmConnect : Form
    {
        private Camera _cameraActiv;
        public Camera cameraActive { 
            get{
                return _cameraActiv;
            }
            set { 
                _cameraActiv = value; 
            } 
        }
        public frmConnect()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //(cek camera)
            if (cameraActive != null)
                cameraActive.Connect(txtAddress.Text, txtUsername.Text, txtPassword.Text);

            Close();
        }
        
    }

    code diatas digunakan untuk konesksi camera,
    dilakukan pengecekan, jika camera aktif maka koneksi camera menggunakan ipaddress, username dan password.

    ipcamera : 192.168.7.36
    username : admin
    password : admin123

    ip camera : 192.168.7.37
    username : admin
    password : hik12345